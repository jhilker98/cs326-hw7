#!/usr/bin/env python
import sys
import pandas as pd
import random as r


  
def is_empty(char):
    if char == "_EMPTY_":
        return ""

def swap(sent, prods):
    while sent in prods.index.str.strip():
        print("starting swap...")
        print("sent is {}".format(sent))
        sent = sent.replace(sent, r.choice(prods[sent].str.strip().values))
        print(sent)
    return sent
    
file_name = str(sys.argv[1])
f = open(file_name)
file_info = []
for line in f:
    file_info.append(line.strip())

variabs = file_info[0].split(",")
terms = file_info[1].split(",")
prods = []
prod_res = []
for line in file_info:
    if line.find("->") != -1:
        prods.append(line.split("->")[0])
        prod_res.append(line.split("->")[1])
    else:
        start_symbol = line.strip()
productions = pd.Series(prod_res,index=prods)
productions.index = productions.index.str.strip()
num_strs = int(sys.argv[2])

new_str = start_symbol
print ("new_str is {}".format(new_str))
new_str = swap(new_str, productions)
print("new_str is now {}".format(new_str))
        
